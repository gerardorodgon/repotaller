var clientesObtenidos;

function getClientes(){
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  //?$filter=UnitPrice gt 15&$orderby=UnitPrice desc&$top=5&$skip=3&$count=true&$select=ProductID, ProductName, UnitPrice";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      console.log(request.responseText);
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }
  request.open("GET", url, true);
  request.send();
  alert("Clientes");
}

function procesarClientes(){
  var JSONClientes = JSON.parse(clientesObtenidos);
  var bandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  // Creata tabla de Clientes
  var divTabla =  document.getElementById("divClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");
  console.log("Numero de Clientes..: " + JSONClientes.value.length);
  for (var i = 0; i < JSONClientes.value.length; i++) {
    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONClientes.value[i].ContactName;
    var columnaPrecio = document.createElement("td");
    columnaPrecio.innerText = JSONClientes.value[i].City;
    var columnaStock = document.createElement("td");
    columnaStock.innerText = JSONClientes.value[i].Country;

    //genera bandera
    var imgBandera = document.createElement("img");
    imgBandera.classList.add("flag");

    if (JSONClientes.value[i].Country == "UK"){
      imgBandera.src =  bandera + "United-Kingdom.png";
    }else {
      imgBandera.src =  bandera + JSONClientes.value[i].Country + ".png";
      }
    var columnaFlag = document.createElement("td");

    columnaFlag.appendChild(imgBandera);

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);
    nuevaFila.appendChild(columnaFlag);

    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
  //console.log(divTabla.value);
}
